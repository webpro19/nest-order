import { Product } from 'src/products/entities/product.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Order } from './order.entity';
@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  id: number;
  @ManyToOne(() => Product, (product) => product.orderItems)
  product: Product;
  @Column()
  name: string;
  @Column({ type: 'float' })
  price: number;
  @Column()
  amount: number;
  @Column({ type: 'float' })
  total: number;
  @ManyToOne(() => Order, (order) => order.orderItems)
  order: Order;
}
